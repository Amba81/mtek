package com.masoud.mtekproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Logscreen extends AppCompatActivity {

    ImageView fb,twitter,gmail,mtek;
    Button logn;
    TextView AlrAcct,Customer,Accidents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logscreen);

        InitElem();
    }

    public void InitElem(){
        fb=(ImageView)findViewById(R.id.fb_logo);
        twitter=(ImageView)findViewById(R.id.twit_logo);
        gmail=(ImageView)findViewById(R.id.gmail_logo);
        mtek=(ImageView)findViewById(R.id.mtekLogo);

        logn = (Button)findViewById(R.id.Login_Btn);

        AlrAcct = (TextView)findViewById(R.id.Text_Account);
        Customer = (TextView)findViewById(R.id.NewCust);
        Accidents = (TextView)findViewById(R.id.Accidents_txt);
    }

    public void ClickLogscreen(View v){
        if(v.getId()==R.id.Login_Btn){
            Intent i = new Intent(Logscreen.this,Logoption.class);
            startActivity(i);
        }
    }
}
